<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "withdrawal_requests".
 *
 * @property int $id
 * @property double $price Сумма
 * @property int $user_id Пользователь
 * @property int $status Пользователь
 * @property string $purse Кошелек
 * @property double $retention_amount Сумма удержания
 * @property string $created_at Дата создание
 * @property string $date_payment Дата и время оплаты
 *
 * @property UsersList $user
 */
class WithdrawalRequests extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'withdrawal_requests';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price', 'retention_amount'], 'number'],
            [['user_id'], 'integer'],
            [['created_at', 'date_payment', 'status'], 'safe'],
            [['purse'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UsersList::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price' => 'Сумма',
            'user_id' => 'Пользователь',
            'purse' => 'Кошелек',
            'retention_amount' => 'Сумма удержания',
            'created_at' => 'Дата создание',
            'date_payment' => 'Дата и время оплаты',
            'status' => 'Статус',
            'type' => 'Тип',
            'dubl' => 'Дубль',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date('Y-m-d H:i:s');
            $this->date_payment = date('Y-m-d H:i:s');
        }
      
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UsersList::className(), ['id' => 'user_id']);
    }

    public function getUsersList()
    {
        $users = UsersList::find()->all();
        return ArrayHelper::map($users,'id','name');
    }
}
