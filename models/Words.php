<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "words".
 *
 * @property int $id
 * @property string $value Значение
 * @property int $position Позиция
 * @property int $langs_id Язык
 *
 * @property Langs $langs
 */
class Words extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'words';
    }
    public $fileUploading;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['value'], 'string'],
            [['value'], 'required'],
            [['position', 'langs_id'], 'integer'],
            [['langs_id'], 'exist', 'skipOnError' => true, 'targetClass' => Langs::className(), 'targetAttribute' => ['langs_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => 'Значение',
            'position' => 'Позиция',
            'langs_id' => 'Язык',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        return $this->hasOne(Langs::className(), ['id' => 'langs_id']);
    }

    public function getLangsList()
    {
        $langs = Langs::find()->all();
        return ArrayHelper::map($langs, 'id', 'name');
    }
}
