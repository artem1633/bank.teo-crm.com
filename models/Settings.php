<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $key Ключ
 * @property string $value Значение
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $fileUploading;

    public static function tableName()
    {
        return 'settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['value'], 'string'],
            [['name', 'key'], 'string', 'max' => 255],
            [['fileUploading'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xls, xlsx',],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'key' => 'Ключ',
            'value' => 'Значение',
            'fileUploading'=>'Выберите файл',
        ];
    }


    /**
     * Ищет запись в БД по ключу
     * @param string $key
     * @return null|static
     */
    public static function findByKey($key)
    {
        return self::findOne(['key' => $key]);
    }

    /*public function getPercentageList()
    {
        return [
            '1' => '1 уровень',
            '2' => '2 уровень',
            '3' => '3 уровень',
            '4' => '4 уровень',
            '5' => '5 уровень',
        ];
    }

    public function getVideoList()
    {
        return [
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
            '6' => '6',
            '7' => '7',
        ];
    }*/
}
