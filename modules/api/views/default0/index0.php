﻿<pre style="word-wrap: break-word; white-space: pre-wrap;">-------------------------------------------------------------------
-----------------API приложения------------------------------------
-------------------------------------------------------------------
Подтверждение СМС - POST
 http://taxi.shop-crm.ru/api/user/gettoken
Принимает
login - Номер телефона водителя


    Или регистрирует или авторизирует пользователя, если он существует.
Возвращает
{
	"success" : {
                 "sms_token" : ['VALUE'],
                }
}

ошибки будут прописаны в статусе error
-------------------------------------------------------------------
-------------------------------------------------------------------
ALARM!ВАЖНО!

    ПОСЛЕ АВТОРИЗАЦИИ/РЕГИСТРАЦИИ СИСТЕМА ВЫДАСТ 'access_token'
    ЕГО НУЖНО ОТПРАВЛЯТЬ СО ВСЕМИ ЗАПРОСАМИ! ИНАЧЕ СИСТЕМА ОТВЕТИТ ОШИБКОЙ!


-------------------------------------------------------------------
-------------------------------------------------------------------
    Авторизация - POST
 http://taxi.shop-crm.ru/api/user/login
Принимает 
login - Номер телефона водителя
device - идентификатор устройства для пуш уведомлений

    Или регистрирует или авторизирует пользователя, если он существует.
Возвращает
{
            'id' : ['ID'],
            'first_name' : ['first_name'],
            'last_name' : [ 'last_name'],
            'patronymic_name' : [ 'patronymic_name'],
            'passport' : [ 'passport'],
            'phone' : [ 'phone'],
            'email' : [ 'email'],
            'city' : [ 'city'],
            'address' : [ 'address'],
            'building' : [ 'building'],
            'car' : [ 'car'],
            'paying_card' : [ 'paying_card'],
            'status' : [ 'status'],
            'avg_mark' : [ 'avg_mark'],
}

ошибки
no param - нет параметров
invalid login or password - неправильный логин или пароль
----------------------НА ДАННЫЙ МОМЕНТ НЕ АКТУАЛЬНО------------------------------------
Регистрация Пользователя - POST
    http://taxi.shop-crm.ru/api/user/signup
    Принимает
    Разделы passport,car можно не указывать, Если нужно сразу создать все -
    то ОБЯЗАТЕЛЬНО в параметр 'driver_id' нужно отправить '1' Параметр означает что Пользователь является водителем

    paying_card можно включать без параметра driver_id

    "user": {
            'first_name' : ['first_name'],
            'last_name' : [ 'last_name'],
            'patronymic_name' : [ 'patronymic_name'],
            'passport' : {
                         'photo_path' :   ['photo_path'],
                         'series' :   ['series'],
                         'number' :   ['number'],
                         'issue_date' :   ['issue_date'],
                         'expiration_date' :   ['expiration_date'],
                         'authority_of_passport' :   ['authority_of_passport'],
                            },
            'phone' : [ 'phone'],
            'email' : [ 'email'],
            'city' : [ 'city'],
            'address' : [ 'address'],
            'building' : [ 'building'],
            'car' : {
                       'mark' :  ['mark'],
                       'model' :  ['model'],
                       'issue_year' :  ['issue_year'],
                       'colour' :  ['colour'],
                       'reg_number' :  ['reg_number'],
                       'kond' :  ['kond'],
                       'smoke' :  ['smoke'],
                       'child_boost' :  ['child_boost'],
                       'animal' :  ['animal'],
                       'business' :  ['business'],
                       'ekonom' :  ['ekonom'],
                               },
            'paying_card' :
            {
                        'number' : ['number'],
                        'expiration_date' : ['number'],
                        'card_holder' : ['number'],
                        'secret_key' : ['number'],
            },
            'driver_id' : ['address'],
	},
}
    Возвращает:
    ошибки
no param - нет параметров
something went's wrong - Не хватает основных параметров или они указаны неверно
Something wen't wrong with file =( - Ошибка при загрузке файла

    -------------------------------------------------------------------
    Поулчить новости
    http://taxi.shop-crm.ru/api/user/getnews

    Возвращает
    'news':{
        'id': ['id'],
        'title': ['title'],
        'text': ['text'],
        'creation_date' : ['creation_date'],
    }



-------------------------------------------------------------------
    Добавление авто вителю - POST
    http://taxi.shop-crm.ru/api/user/createcar

               'user_id' : ['user_id'],
               'mark' :  ['mark'],
               'model' :  ['model'],
               'issue_year' :  ['issue_year'],
               'colour' :  ['colour'],
               'reg_number' :  ['reg_number'],
               'kond' :  ['kond'],
               'smoke' :  ['smoke'],
               'child_boost' :  ['child_boost'],
               'animal' :  ['animal'],
               'business' :  ['business'],
               'ekonom' :  ['ekonom'],
        Возвращает:
    ошибки
no param - нет параметров
something went's wrong - Не хватает основных параметров или они указаны неверно

--------------------------------------------------------------------
    Добавление платёжной карты пользователю - POST
    http://taxi.shop-crm.ru/api/user/createcard


    'paying_card' :
            {
            'user_id' : [user_id],
            'number' : ['number'],
            'expiration_date' : ['number'],
            'card_holder' : ['number'],
                            }
        Возвращает:
    ошибки
no param - нет параметров
something went's wrong - Не хватает основных параметров или они указаны неверно

--------------------------------------------------------------------
    Добавление пасспорта водителю- POST
    http://taxi.shop-crm.ru/api/user/createpassport

             'user_id' : ['user_id'],
             'photo_path' :   ['photo_path'],
             'series' :   ['series'],
             'number' :   ['number'],
             'issue_date' :   ['issue_date'],
             'expiration_date' :   ['expiration_date'],
             'authority_of_passport' :   ['authority_of_passport'],

        Возвращает:
    ошибки
no param - нет параметров
something went's wrong - Не хватает основных параметров или они указаны неверно
    
--------------------------------------------------------------------
Выход из сессии пользователя - POST
http://taxi.shop-crm.ru/api/user/logout

Принимает 
user_id - номер водителя
Возвращает
 {"logout": "success"}
ошибки
no param - нет параметров
-------------------------------------------------------------------
Создание заявки пользователя - POST
http://taxi.shop-crm.ru/api/user/sendrequest

    Принимает

    "user_id":,
    "city": ,
    "address": ,
    "building":,
    "price":
    'smoke' : 'Салон для курящих',
    'child_boost' : 'Детское кресло или бустер',
    'animal' : 'Перевозка животного',
    'business' : 'Бизнес',
    'ekonom' : 'Эконом',
    'pre_date' : 'Дата поездки',

    Возвращает инфу о заявке или ошибку
-------------------------------------------------------------------
    Удаление заявки пользователя - POST
   http://taxi.shop-crm.ru/api/user/deleterequest

    Принимает
    'user_id'
    'request_id'

    Возвращает success или error с ошибкой.
-------------------------------------------------------------------

    Приём заявки водителем - POST
    http://taxi.shop-crm.ru/api/user/acceptrequest

    Принимает
    'user_id'
    'request_id'

    Возвращает информацию о принятой заявке и созданной поездке или error с ошибкой.
-------------------------------------------------------------------

    Старт поездки - POST
    http://taxi.shop-crm.ru/api/user/starttrip

    Принимает
    'user_id'
    'request_id'

    Возвращает информацию о успешно начатой поездке или ошибку с информацией.
-------------------------------------------------------------------

    Получить список заявок - POST
    http://taxi.shop-crm.ru/api/user/getrequestlist
    Принимает

    'user_id'

    Возвращает список свободных заявок в этом городе.
-------------------------------------------------------------------

    Завершить поездку - POST
    http://taxi.shop-crm.ru/api/user/endtrip
    Принимает

    'user_id','request_id'

    завершает поездку возвращает статус
-------------------------------------------------------------------

    Получить список поездок - POST
    http://taxi.shop-crm.ru/api/user/gettrips
    Принимает

    'user_id'

    Возвращает список поездок и информацию
-------------------------------------------------------------------

    Оценить водителя - POST
    http://taxi.shop-crm.ru/api/user/markdriver
    Принимает

    'trip_id','mark','comment'

    Оценка от 1 до 5, комментарий может быть или нет.
    Возвращает успешный результат или ошибку с описанием.

-------------------------------------------------------------------

    Получить комментарии - POST
    http://taxi.shop-crm.ru/api/user/getreviews
    Принимает

    'user_id'


    Возвращает успешный результат или ошибку с описанием.

-------------------------------------------------------------------

    Отправить жалобу - POST
    http://taxi.shop-crm.ru/api/user/sendcomplaint
    Принимает
phone - тот на кого жалоба
    'user_id','phone','comment'

    Создаёт жалобу от user_id на второго участника поездки.

    Возвращает успешный результат или ошибку с описанием.

-------------------------------------------------------------------

    Редактировать профиль - POST
    http://taxi.shop-crm.ru/api/user/editprofile
    Принимает

    'user_id'
    и любой набор из атрибутов

    "first_name" => 'string',
    "last_name" => 'string',
    "patronymic_name" => 'string',
    "email" => 'email',
    "city" => 'EXISTING_city',
    "address" => 'string',
    "building" => 'integer',
    "avatar" => 'avatar',
    "imageFile1" => 'imageFile1',
    "imageFile2" => 'imageFile2',
    "imageFile3" => 'imageFile3',

    Возвращает успешный результат или ошибку с описанием.
------------------------------------------------------------
    Получить инфо о профиле
http://taxi.shop-crm.ru/api/user/getinfo
    Принимает user_id

-------------------------------------------------------------------
    РЕДАКТИРОВАНИЕ\УДАЛЕНИЕ авто - POST
    http://taxi.shop-crm.ru/api/user/editcar

    Если параметр delete - true то удалит авто, в противном случае не указывать параметр!

    Принимает:
                'delete' : ['true'],
               'user_id' : ['user_id'],
               'mark' :  ['mark'],
               'model' :  ['model'],
               'issue_year' :  ['issue_year'],
               'colour' :  ['colour'],
               'reg_number' :  ['reg_number'],
               'kond' :  ['kond'],
               'smoke' :  ['smoke'],
               'child_boost' :  ['child_boost'],
               'animal' :  ['animal'],
               'business' :  ['business'],
               'ekonom' :  ['ekonom'],
        Возвращает:

               'id' : ['id'],
               'mark' :  ['mark'],
               'model' :  ['model'],
               'issue_year' :  ['issue_year'],
               'colour' :  ['colour'],
               'reg_number' :  ['reg_number'],
               'kond' :  ['kond'],
               'smoke' :  ['smoke'],
               'child_boost' :  ['child_boost'],
               'animal' :  ['animal'],
               'business' :  ['business'],
               'ekonom' :  ['ekonom'],
    ошибки
no param - нет параметров
something went's wrong - Не хватает основных параметров или они указаны неверно

--------------------------------------------------------------------
    РЕДАКТИРОВАНИЕ\УДАЛЕНИЕ пасспорта водителю- POST
    http://taxi.shop-crm.ru/api/user/editpassword

    Если параметр delete - true то удалит авто, в противном случае не указывать параметр!

    Принимает:
             'delete' : ['true'],
             'user_id' : ['user_id'],
             'photo_path' :   ['photo_path'],
             'series' :   ['series'],
             'number' :   ['number'],
             'issue_date' :   ['issue_date'],
             'expiration_date' :   ['expiration_date'],
             'authority_of_passport' :   ['authority_of_passport'],
             'imageFile' :   ['imageFile'],

        Возвращает:
             'id' : ['id'],
             'photo_path' :   ['photo_path'],
             'series' :   ['series'],
             'number' :   ['number'],
             'issue_date' :   ['issue_date'],
             'expiration_date' :   ['expiration_date'],
             'authority_of_passport' :   ['authority_of_passport'],
             'imageFile' :   ['imageFile'],
    ошибки
no param - нет параметров
something went's wrong - Не хватает основных параметров или они указаны неверно
--------------------------------------------------------------------
    Получение сообщений
    http://taxi.shop-crm.ru/api/user/getmessages


    Принимает 'user_id'

    Возвращает список сообщений.
    Или ошибку с описанием
--------------------------------------------------------------------
    Отправить сообщение
    http://taxi.shop-crm.ru/api/user/sendmessage


    Принимает 'user_id','message'

    Возвращает success с атрибутами сообщения.
    Или ошибку с описанием


--------------------------------------------------------------------
    http://taxi.shop-crm.ru/api/user/finddrivers

    принимает user_id,access_token

    Возвращает ошибки или водителей online в пределах радиуса заявки данного города

_____________________________________________________________________


    http://taxi.shop-crm.ru/api/user/setcoords

    принмиает user_id,access_token,x,y

    Устанавливает координаты пользователя

    возвращает инфу о пользователе или ошибки.
_____________________________________________________________________


    http://taxi.shop-crm.ru/api/user/setstatus

    принмиает user_id,status,access_token
    Если статус = 1 - то делает статус водителя free, в противном случае - busy


    возвращает инфу о пользователе или ошибки.
_____________________________________________________________________


    http://taxi.shop-crm.ru/api/user/getcities

    принмиает user_id,access_token

    возвращает список городов или ошибки.
__________________________________________________________________________

    http://taxi.shop-crm.ru/api/user/getcomplaint

    принмиает user_id,access_token

    возвращает жалобы или ошибки.
__________________________________________________________________________

    http://taxi.shop-crm.ru/api/user/rejectrequest

    принмиает user_id,request_id,access_token

    Отказывается водитель от заявки, статус возвращается в свободен, статус заявки в 0


    возвращает заявку или ошибки.
__________________________________________________________________________

    http://taxi.shop-crm.ru/api/user/onplace

    принмиает user_id,request_id,access_token

    Водитель прибыл на место. Меняет статус заявки

    возвращает заявку или ошибки.
__________________________________________________________________________

    http://taxi.shop-crm.ru/api/user/finddriversopt

    принмиает user_id,request_id,access_token

    Возвращает всех водителей в радиусе заявки по данному городу, у которых параметры АВТО совадают с параметрами ЗАЯВКИ

    возвращает водителей с инфо о авто или ошибки.
__________________________________________________________________________

    http://taxi.shop-crm.ru/api/user/getrequests

    принмиает user_id,access_token,type,is_driver

    is_driver - если 1 то Водитель, если 0 то клиент
    type - если 0 - новые заявка, если 1 то заверщённые

    возвращает заявки или ошибки.
__________________________________________________________________________

    http://taxi.shop-crm.ru/api/user/acceptprice

    Клиент соглашается на цену, которую установил водитель в методе sendownprice.

    принмиает user_id,access_token,request_id,price

    user_id - Клиент
    price - цена на которую согласился
    request_id - заказ

    Придёт ошибка или success, а также пуш уведомление с type = 4 для водителя

    Цена в заказе обновится.
__________________________________________________________________________

    http://taxi.shop-crm.ru/api/user/sendownprice

    Водитель отправляет свою цену на данный заказ.

    принмиает user_id,access_token,request_id,price

    user_id - Водитель
    price - цена которую хочет
    request_id - заказ

    Клиенту в заказе придёт уведомление пуш с инфой о заказе, водителе и т.д.  с type = 2

__________________________________________________________________________

    http://taxi.shop-crm.ru/api/user/sendtenprice

    Клиент поднимает цену на свой заказ на 10 рублей.


    принмиает user_id,access_token,request_id,price

    user_id - клиент
    request_id - заказ

    Поднимает цену в заказе на 10 рублей.


    Всем водителям в радиусе действия заявки приходит пуш о подъёме цены  с type = 3


    возвращает заявки или ошибки.
__________________________________________________________________________

    http://taxi.shop-crm.ru/api/user/confirmcar

    принмиает user_id,request_id,access_token

    Клиент подтвердил прибытие авто. Меняет статус заявки на 3 присылает пуш водителю с реквестом, клиентом, и type = 4

    возвращает заявку или ошибки.

__________________________________________________________________________

    http://taxi.shop-crm.ru/api/user/sendreqeustto

    принмиает user_id,request_id,driver_id,access_token

    Отправляет заказ водителю с driver_id, пуш внутри с request,type=5,client

    возвращает заявку или ошибки.
__________________________________________________________________________

    http://taxi.shop-crm.ru/api/user/buyturn
    GET запрос
    принмиает user_id,access_token,turn_id

    Покупает и привязывает смену к водителю, у водителя изеняется баланс. Цена смены берётся из
    поля price в смене.

    возвращает заявку или ошибки.
__________________________________________________________________________

    http://taxi.shop-crm.ru/api/user/getturns
    GET запрос
    принмиает user_id,access_token

    Возвращает список доступных для водителя смен(по городу) available

    возвращает смены или ошибки.
__________________________________________________________________________

    http://taxi.shop-crm.ru/api/user/getcurrentturn
  GET запрос
    принмиает user_id,access_token

    Возвращает водителю текущую смену ИЛИ пустую строку, если время действия смены закончилось.

    возвращает смены или ошибки.

__________________________________________________________________________
api/help/help
GET запрос
Принимает файл
    int user_id
    string access_token
-----
Возвращает
{article}
<i style="color: gray;">
{
  "id": 3,
  "tag": "offer",
  "title": "ПОМОЩЬ",
  "content": "<ol>\r\n\t<li><strong>Как пополнить баланс в программе</strong></li>\r\n\t<li><strong>Что делать если я создал заказ а водитель так и не назначен</strong></li>\r\n</ol>\r\n",
  "date_create": null,
  "date_update": "2017-09-27 12:00:13"
}
</i>
__________________________________________________________________________
api/help/rules
GET запрос
Принимает файл
    int user_id
    string access_token
-----
Возвращает
{article}
<i style="color: gray;">
{
  "id": 3,
  "tag": "offer",
  "title": "ПРАВИЛА",
  "content": "<ol>\r\n\t<li><strong>Как пополнить баланс в программе</strong></li>\r\n\t<li><strong>Что делать если я создал заказ а водитель так и не назначен</strong></li>\r\n</ol>\r\n",
  "date_create": null,
  "date_update": "2017-09-27 12:00:13"
}
</i>
__________________________________________________________________________
api/help/offer
GET запрос
Принимает файл
    int user_id
    string access_token
-----
Возвращает
{article}
<i style="color: gray;">
{
  "id": 3,
  "tag": "offer",
  "title": "ОФЕРТА",
  "content": "<ol>\r\n\t<li><strong>Как пополнить баланс в программе</strong></li>\r\n\t<li><strong>Что делать если я создал заказ а водитель так и не назначен</strong></li>\r\n</ol>\r\n",
  "date_create": null,
  "date_update": "2017-09-27 12:00:13"
}
</i>

__________________________________________________________________________
</pre>