<?php

namespace app\controllers;

use app\models\UsersList;
use Yii;
use app\models\Users;
use app\models\UsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    public function actionDashboard()
    {
        $request = Yii::$app->request;
        $date_start = $request->get('start') ?? date('Y-m-d 00:00:00', time());
        $date_end = $request->get('end') ?? date('Y-m-d 23:59:59', time());


        return $this->render('dashboard', [
            'questionaryCount' => 0,
            'resumeCount' => 0,
            'resumeTodayCount' => 0,
            'sendCount' => 0,
            'register_info' => UsersList::getRegisteredInfo($date_start, $date_end),
            'subscribe_info' => UsersList::getSubscribedInfo($date_start, $date_end),
            'amount' => UsersList::getAmountInfo(),
        ]);
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Users model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Users();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'size' => 'normal',
                    'title' => "Создать",
                    'content' => '<span class="text-success">Успешно выпольнено</span>',
                    'footer' => Html::button('Ок',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Создать ещё', ['create'], ['class' => 'btn btn-info', 'role' => 'modal-remote'])

                ];
            } else {
                return [
                    'title' => "Создать",
                    'size' => 'large',
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing Users model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->load($request->post()) && $model->save()) {
                return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
            } else {
                return [
                    'title' => $model->getPermission(),
                    'size' => 'large',
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Creates a new Users model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionAvatar()
    {
        $request = Yii::$app->request;
        $id = Yii::$app->user->identity->id;
        $model = Users::findOne(Yii::$app->user->identity->id);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->validate()) {

                $model->file = UploadedFile::getInstance($model, 'file');
                if (!empty($model->file)) {
                    $model->file->saveAs('avatars/' . $id . '.' . $model->file->extension);
                    Yii::$app->db->createCommand()->update('users', ['foto' => $id . '.' . $model->file->extension],
                        ['id' => $id])->execute();
                }

                return [
                    'forceReload' => '#profile-pjax',
                    'size' => 'normal',
                    'title' => "Аватар",
                    'forceClose' => true,
                ];
            } else {
                return [
                    'title' => "Аватар",
                    'size' => 'small',
                    'content' => $this->renderAjax('avatar', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            }
        }

    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionChange($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {

                return [
                    'forceClose' => true,
                    'forceReload' => '#profile-pjax',
                ];
            } else {
                return [
                    'title' => "Изменить",
                    'size' => "large",
                    'content' => $this->renderAjax('change', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])
                ];
            }
        } else {
            echo "ddd";
        }
    }

    public function actionProfile()
    {
//        $request = Yii::$app->request;
        return $this->render('profile', [
        ]);
    }

    /**
     * Delete an existing Users model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        if ($id != 1) {
            $this->findModel($id)->delete();
        }

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete multiple existing Vacancy model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            if ($pk != 1) {
                $model->delete();
            }
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSelectDate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'title' => 'Укажите период',
            'content' => $this->renderAjax('_select_date', [
                'model' => new Users(),
            ]),
            'footer' => Html::a('Выбрать', ['#'], [
                'id' => 'set-period',
                'class' => 'btn btn-primary'
            ])
        ];
    }

    public function actionGetPeriodInfo()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;

        $start = $request->get('start');
        $end = $request->get('end');

        $register_count = UsersList::find()
                ->andWhere(['BETWEEN', 'create_at', $start, $end])
                ->count() ?? 0;

        $subscribed_pay = UsersList::find()
            ->andWhere(['BETWEEN', 'pay_subscribe_date', $start, $end])
            ->count() ?? 0;

        return [
            'success' => 1,
            'data' => [
                'register' => $register_count,
                'pay' => $subscribed_pay,
            ]
        ];
    }
}
