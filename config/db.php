<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=127.0.0.1;port=3306;dbname=bank.teo-crm.com',
    'username' => 'bank.teo-crm.com',
    'password' => 'bank.teo-crm.com',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,

    //'schemaCache' => 'cache',
];
