<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Words */
?>
<div class="words-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'value:ntext',
            'position',
            'langs_id',
        ],
    ]) ?>

</div>
