<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Words */

?>
<div class="words-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
