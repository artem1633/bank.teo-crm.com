<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UsersList */
?>
<div class="users-list-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
