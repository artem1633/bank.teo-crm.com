<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use app\models\Chat;

CrudAsset::register($this);
$this->title = 'Просмотр';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="users-page-index">
    <div class="question-container">                            
        <div class="row">
            <div class="col-md-7">                      
                <div class="panel panel-success panel-hidden-controls" >
                    <div class="panel-heading ui-draggable-handle">
                        <h1 class="panel-title"> <b>Карточка</b> </h1>
                        <?= Html::a('Редактировать &nbsp;<i class="glyphicon glyphicon-pencil"></i>', ['/users-list/update', 'id' => $model->id,], [ 'role'=>'modal-remote','title'=> 'Редактировать','class'=>'btn btn-warning btn-rounded pull-right',]) ?>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-up"></span></a></li>
                        </ul>                                
                    </div>
                    <div class="panel-body">
                        <div style="max-height: 600px; overflow: auto; display: flex; justify-content: space-between;"> 
                            <?php Pjax::begin(['enablePushState' => false, 'id' => 'crud-datatable-pjax']) ?>
                            <div>
                                <p><b><?=$model->getAttributeLabel('name')?></b> : <?=$model->name ?></p>
                                <p><b><?=$model->getAttributeLabel('language_id')?></b> : <?=$model->getLanguagesList()[$model->language_id] ?></p>
                                <p><b><?=$model->getAttributeLabel('telegram_id')?></b> : <?=$model->telegram_id ?></p>
                                <p><b><?=$model->getAttributeLabel('country')?></b> : <?=$model->country ?></p>
                                <p><b><?=$model->getAttributeLabel('city')?></b> : <?=$model->city ?></p>
                                <p><b><?=$model->getAttributeLabel('balance')?></b> : <?=$model->balance ?></p>
                                <p><b><?=$model->getAttributeLabel('users_cash')?></b> : <?=$model->users_cash ?></p>
                                <p><b><?=$model->getAttributeLabel('partner_code')?></b> : <?=$model->partner_code ?></p>
                                <p><b><?=$model->getAttributeLabel('who_invited')?></b> : <?=$model->who_invited ?></p>
                            </div>
                            <?php Pjax::end() ?>
                        </div>                                       
                    </div>                           
                </div>                   
            </div>

                <div class="col-md-5">  
                    <div class="col-md-12">
                        <div class="panel panel-warning panel-toggled">
                            <div class="panel-heading ui-draggable-handle">
                                <h3 class="panel-title"><b>Чат</b></h3>
                                <ul class="panel-controls">
                                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-up"></span></a></li>
                                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                </ul>
                            </div>
                            <div class="panel-body">
                                <div class="content-frame-body content-frame-body-left" style="max-height: 400px; overflow: auto; " >
                                    <div class="messages messages-img">
                                        <?php 
                                        foreach ($chatText as $value) {
                                            $identity = Yii::$app->user->identity;
                                            if($value->user_id == $identity->id) $in = '';
                                            else $in = 'in';
                                            $path = 'http://' . $_SERVER['SERVER_NAME'].'/examples/images/users/avatar.jpg';
                                        ?>
                                        <div class="item <?=$in?> item-visible">
                                            <div class="image">
                                                <img src="<?=$path?>" alt="<?=$value->user->name?>">
                                            </div>
                                            <div class="text">
                                                <div class="heading">
                                                    <a href="#"><?=$value->user->name?></a>
                                                    <span class="date"><?= date( 'H:i:s d.m.Y', strtotime($value->date_time) ) ?></span>
                                                </div>
                                                <?=$value->text?>
                                            </div>
                                        </div>
                                        <?php } ?>                            
                                    </div>                                    
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="panel panel-default push-up-10">
                                    <div class="panel-body panel-body-search">
                                        <?php $form = ActiveForm::begin(); ?>
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <button class="btn btn-warning"><span class="fa fa-camera"></span></button>
                                                    <button class="btn btn-danger"><span class="fa fa-chain"></span></button>
                                                </div>
                                                <input type="text" name="text" class="form-control" placeholder="Написать сообщение...">
                                                <div class="input-group-btn">
                                                    <button class="btn btn-info">Отправить</button>
                                                </div>
                                            </div>
                                        <?php ActiveForm::end(); ?> 
                                    </div>
                                </div>
                            </div>
                        </div>

                                                
                    </div>
                </div>

                

            </div>
        </div>

</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

<?php 
/*$chat = Chat::find()->where(['chat_id' => '#resume-'.$model->id, 'is_read' => 0 ])->andWhere([ '!=', 'user_id', Yii::$app->user->identity->id ])->one();
if($chat != null){
    $chat->is_read = 1;
    $chat->save();
}*/

?>