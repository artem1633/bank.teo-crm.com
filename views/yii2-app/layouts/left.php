<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\helpers\Url;
use app\models\Users;
use yii\widgets\Pjax;

$path = 'http://' . $_SERVER['SERVER_NAME'].'/examples/images/users/avatar.jpg';
$pathInfo = Yii::$app->request->pathInfo;
$class = "";
if($pathInfo == 'name/index' || $pathInfo == 'surname/index' || $pathInfo == 'additional-information/index' || $pathInfo == 'user-agent/index') $class="active";

$classReport = "";
if($pathInfo == 'users/dashboard' || $pathInfo == 'logs/index' || $pathInfo == '') $classReport = "active";
?>
<?php 
        $session = Yii::$app->session;
        if($session['left'] == 'small') $left="x-navigation-minimized";
        else $left="x-navigation-custom";
    ?>
<!-- START PAGE SIDEBAR -->
            <div class="page-sidebar page-sidebar-fixed scroll">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation <?=$left?>">
                    <li class="xn-logo">
                        <a href="<?=Url::toRoute([Yii::$app->homeUrl])?>"><?=Yii::$app->name?></a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li>
                        <?= Html::a('<span class="fa fa-users"></span> <span class="xn-text">Пользователи</span>', ['/users-list/index'], []); ?>
                    </li>
                    <li>
                        <?= Html::a('<span class="fa fa-tasks"></span> <span class="xn-text">Покупка</span>', ['/withdrawal-requests/index'], []); ?>
                    </li>
                    <?php if(Yii::$app->user->identity->type == 0) { ?>
                    <li>
                        <?= Html::a('<span class="fa fa-cogs"></span> <span class="xn-text">Настройки</span>', ['/settings/index'], []); ?>
                    </li>
                    <?php } ?>

                    <li class="xn-openable <?=$class?>"  >
                        <a href="#"><span class="fa fa-bar-chart-o"></span><span class="xn-text">Справочники</span></a>
                        <ul>

                            <li>
                                <?= Html::a('<span class="fa fa-users"></span> <span class="xn-text">Пользователи</span>', ['/users/index'], []); ?>
                            </li>
                            <li>
                                <?= Html::a('<span class="fa fa-tasks"></span> <span class="xn-text">Слова</span>', ['/words/index'], []); ?>
                            </li>
                            <li>
                                <?= Html::a('<span class="fa fa-tasks"></span> <span class="xn-text">Язык</span>', ['/langs/index'], []); ?>
                            </li>
                        </ul>
                    </li>

                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->