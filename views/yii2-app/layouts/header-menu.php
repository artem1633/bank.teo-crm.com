<?php
use yii\helpers\Html;
use yii\helpers\Url;

?>

<header >
    <div class="wrapper">
        <div class="logo">
            <a href="<?=Url::toRoute([Yii::$app->homeUrl])?>"><h2><?=Yii::$app->name?></h2></a>
        </div>
        <div class="hdr_menu">            
        </div>
        <div class="hdr_rght">            
        </div>
        <div class="hamburger">
            <span></span>
            <span></span>
            <span></span>
        </div>            
    </div>         
</header>