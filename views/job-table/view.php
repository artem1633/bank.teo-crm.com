<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\JobTable */
?>
<div class="job-table-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'date',
            'time',
            'value',
        ],
    ]) ?>

</div>
