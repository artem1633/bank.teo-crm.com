<?php

use johnitvn\ajaxcrud\CrudAsset;
use yii\bootstrap\Modal;
use yii\helpers\Html;

/** @var  $questionaryCount */
/** @var  $resumeCount */
/** @var  $resumeTodayCount */
/** @var  $sendCount */
/** @var array $register_info Информация по зарегистрированным акааунтам
 *  [
 * 'count' => 'Общее кол-во зарегистрированных',
 * 'period' => [
 *          'start' => 'Начало периода',
 *          'end' => 'Конец периода',
 *          'count' => 'Количиество зарегистрировавшихся за период',
 *      ]
 * ]
 */
/** @var array $subscribe_info Информация по подпискам
 *  [
 * 'active' => 'Количество пользователей с активной подпиской',
 * 'purchased' => [
 *              'start' => 'Начало периода',
 *              'end' => 'Конец периода',
 *              'count' => 'Кол-во человек, купивших подписку за период',
 *         ]
 * ]
 */
/** @var  array $amount Информация о движении средств
 *  [
 *    'received' => [
 *          'Валюта' => общая суммма по валюте
 *          'Валюта2' => общая суммма по валюте
 *      ],
 *    'payable' => 'Подлежит выплате по МЛМ'
 * ]
 */

CrudAsset::register($this);

$this->title = 'Рабочий стол';
?>
    <div class="site-index">
        <div class="row">
            <div class="col-xs-12">
                <button type="button" class="btn btn-xs btn-warning" onclick='startIntro();'>Начать обучение</button>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-4">
                <div class="widget widget-warning widget-item-icon">
                    <div class="widget-item-left">
                        <span class="fa fa-users"></span>
                    </div>
                    <div class="widget-data">
                        <div class="widget-int num-count"><?= $register_info['count']; ?></div>
                        <div class="widget-title">Зарегистрированных</div>
                    </div>
                    <div class="widget-controls">
                        <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip"
                           data-placement="top" title="Удалить виджет"><span class="fa fa-times"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="widget widget-warning widget-item-icon">
                    <div class="widget-item-left">
                        <span class="fa fa-thumbs-o-up"></span>
                    </div>
                    <div class="widget-data">
                        <div class="widget-int num-count"><?= $subscribe_info['active']; ?></div>
                        <div class="widget-title">С активной подпиской</div>
                    </div>
                    <div class="widget-controls">
                        <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip"
                           data-placement="top" title="Удалить виджет"><span class="fa fa-times"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="widget widget-warning widget-item-icon">
                    <div class="widget-item-left">
                        <span class="fa fa-money"></span>
                    </div>
                    <div class="widget-data">
                        <div class="widget-int num-count"><?php
                            foreach ($amount['received'] as $key => $value) {
                                if ($key != 'all') {
//                                    echo strtoupper($key) . ' - ' . $value . '<br>';
                                    echo $value . '&nbsp;<span class="fa fa-usd"></span><br>';
                                }
                            }
                            ?>
                        </div>
                        <div class="widget-title">Поступило средств</div>
                    </div>
                    <div class="widget-controls">
                        <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip"
                           data-placement="top" title="Удалить виджет"><span class="fa fa-times"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="widget widget-warning widget-item-icon">
                    <div class="widget-item-left">
                        <span class="fa fa-arrow-circle-down"></span>
                    </div>
                    <div class="widget-data">
                        <div class="widget-int num-count"><?= $amount['payable']; ?></div>
                        <div class="widget-title">Подлежит выплате по&nbsp;МЛМ</div>
                    </div>
                    <div class="widget-controls">
                        <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip"
                           data-placement="top" title="Удалить виджет"><span class="fa fa-times"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="widget widget-warning widget-item-icon">
                    <div class="widget-item-left">
                        <span class="fa fa-arrow-circle-up"></span>
                    </div>
                    <div class="widget-data">
                        <div class="widget-int num-count"><?= $amount['received']['all'] - $amount['payable']; ?>&nbsp;<span class="fa fa-usd"></span></div>
                        <div class="widget-title">Доход</div>
                    </div>
                    <div class="widget-controls">
                        <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip"
                           data-placement="top" title="Удалить виджет"><span class="fa fa-times"></span></a>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-8">
                <div class="widget widget-warning widget-item-icon">
                    <div class="widget-item-left">
                        <span class="fa fa-calendar"></span>
                    </div>
                    <div class="widget-data widget-data-custom">
                        <div class="dates">
                            <div class="inputs">
                                <div class="date-input">
                                    <label for="start-date">С&nbsp;</label>
                                    <input id="start-date" type="date" class="form-control">
                                </div>
                                <div class="date-input">
                                    <label for="end-date">По</label>
                                    <input id="end-date" type="date" class="form-control">
                                </div>
                            </div>
                            <?= Html::button('Показать', [
                                'class' => 'btn btn-primary',
                                'id' => 'show-btn',
                            ]) ?>
                        </div>
                        <div class="info">
                            <div class="widget-int num-count" id="register-period">0</div>
                            <div class="widget-title">Пользователей <br> зарегистрировано</div>
                        </div>
                        <div class="info">
                            <div class="widget-int num-count" id="pay-subscribe">0</div>
                            <div class="widget-title">Подписок куплено</div>
                        </div>
                    </div>
                    <div class="widget-controls">
                        <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip"
                           data-placement="top" title="Удалить виджет"><span class="fa fa-times"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>

<?php
$script = <<<JS
 $( document ).ready(function() {
     $(document).on('click', '#show-btn', function() {
         var start = $('#start-date').val();
         var end = $('#end-date').val();
         if (!start || !end) alert('Не выбрано конечное или начальное значение периода');
         $.get(
             '/users/get-period-info',
             {
                 start: start,
                 end: end
             },
             function(response) {
                if (response['success'] === 1){
                    var data = response['data'];
                    $('#register-period').html(data['register']);
                    $('#pay-subscribe').html(data['pay'])
                }                 
             }
         )
     })
 });
JS;
$this->registerJs($script);

