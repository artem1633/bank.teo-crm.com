<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var $model \app\models\Users */

if (!$model->end_date){
    $model->end_date = date('Y-m-d', time());
}
$form = ActiveForm::begin(); ?>


<?= $form->field($model, 'start_date')->textInput(['type' => 'date']) ?>
<?= $form->field($model, 'end_date')->textInput(['type' => 'date']) ?>

<?php if (!Yii::$app->request->isAjax) { ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
<?php } ?>

<?php ActiveForm::end(); ?>