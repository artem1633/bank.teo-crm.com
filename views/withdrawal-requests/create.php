<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\WithdrawalRequests */

?>
<div class="withdrawal-requests-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
