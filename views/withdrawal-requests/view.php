<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\WithdrawalRequests */
?>
<div class="withdrawal-requests-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'price',
            'user_id',
            'purse',
            'retention_amount',
            'created_at',
            'date_payment',
        ],
    ]) ?>

</div>
