<?php

use yii\bootstrap\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'width' => '150px',
        'vAlign'=>'middle',
        'template' => '{dubl} {export} {status}',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'buttons'=>[
            'dubl' => function ($url, $model, $key) {
                if ($model->dubl == 1) {
                    return Html::a('Дубль',  '#', [
                        'class' => 'btn btn-danger btn-xs',
                        'role' => 'modal-remote',
                    ]);
                }

            },
            'export' => function ($url, $model, $key) {
                $out = '';
                if ($model->status == 0) {
                    return Html::a('<span class="glyphicon glyphicon-flag"></span>', ['send-pay', 'id' => $model->id], [
                        'class' => 'btn btn-success btn-xs',
                        'role' => 'modal-remote',
                    ]);
                }
            },
            'status' => function ($url, $model, $key) {
                if ($model->status == 0 and $model->type != 1) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', 'https://www.blockchain.com/btc/tx/'.$model->purse, [
                        'class' => 'btn btn-info btn-xs',
                        'target' => '_blank',
                    ]);
                }
            },
        ],
    ],
     [
     'class'=>'\kartik\grid\DataColumn',
     'attribute'=>'id',
     ],
     [
     'class'=>'\kartik\grid\DataColumn',
     'attribute'=>'type',
         'content' => function($data){
            if ($data->type == 1){
                return 'покупка';
            }
             return 'продажа';
         }
     ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'price',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_id',
        'content' => function($data){
            return $data->user->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'purse',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'retention_amount',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_at',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date_payment',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => '{update} {delete}',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Are you sure?',
            'data-confirm-message'=>'Are you sure want to delete this item'],
    ],

];   