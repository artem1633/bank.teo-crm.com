       



        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading ui-draggable-handle">
                        <h3 class="panel-title">Basic Modals</h3>
                    </div>
                    <div class="panel-body" style="height: 500px;">
                        <img src="/img/backgrounds/wall_1.jpg" class="img-cen"> 
                        <span class="fa fa-search-plus search-form"></span>  
                    </div>
                    <div class="next-bot">
                        <a href="#">Далее</a>
                    </div>
                </div>
            </div>
                <div class="col-3">
                            <!-- CONTACT ITEM -->
                    <div class="panel panel-default" style="width: 25%">
                        <div class="panel-body" style="background-color: #3FC5FA; color: white;">
                            <div class="profile-controls">
                                <a href="#" class="profile-control-right"><span class="glyphicon glyphicon-off" style="font-size: 25px; color: white;"></span></a>
                                <div class="widget-big-int plugin-clock pull-right" style="font-size: 20px">19<span>:</span>34</div>
                            </div>
                            
                        </div>                                
                        <div class="panel-body " style="background-color: #3FC5FA; height: 495px;">   
                            <ul class="test-count">
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3 </a></li>
                                <li><a href="#">4 </a></li>
                                <li><a href="#">5 </a></li>
                                <li><a href="#">6 </a></li>
                                <li><a href="#">7 </a></li>
                                <li><a href="#">9 </a></li>
                                <li><a href="#">10 </a></li>
                            </ul>                                 
                        </div>
                        <div class="present-lod">
                            <b>Пройдено:</b>
                        </div>
                       <div class="loading-pres">
                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">60%
                            </div>
                        </div>                              
                    </div>
                </div>
        </div>