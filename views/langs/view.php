<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Langs */
?>
<div class="langs-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'status',
        ],
    ]) ?>

</div>
